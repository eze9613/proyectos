USE [master]
GO
/****** Object:  Database [CorreoExpressBBDD]    Script Date: 12/17/2019 6:00:29 PM ******/
CREATE DATABASE [CorreoExpressBBDD]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CorreoExpressBBDD', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CorreoExpressBBDD.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'CorreoExpressBBDD_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\CorreoExpressBBDD_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [CorreoExpressBBDD] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CorreoExpressBBDD].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CorreoExpressBBDD] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET ARITHABORT OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CorreoExpressBBDD] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CorreoExpressBBDD] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CorreoExpressBBDD] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CorreoExpressBBDD] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET RECOVERY FULL 
GO
ALTER DATABASE [CorreoExpressBBDD] SET  MULTI_USER 
GO
ALTER DATABASE [CorreoExpressBBDD] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CorreoExpressBBDD] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CorreoExpressBBDD] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CorreoExpressBBDD] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CorreoExpressBBDD] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CorreoExpressBBDD', N'ON'
GO
ALTER DATABASE [CorreoExpressBBDD] SET QUERY_STORE = OFF
GO
USE [CorreoExpressBBDD]
GO
/****** Object:  Table [dbo].[dis]    Script Date: 12/17/2019 6:00:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dis](
	[iddistancia] [int] IDENTITY(1,1) NOT NULL,
	[distancia] [varchar](20) NULL,
	[precio_dis] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[iddistancia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[emisor]    Script Date: 12/17/2019 6:00:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[emisor](
	[idsolicitud] [int] IDENTITY(1,1) NOT NULL,
	[nomb_emis] [varchar](50) NULL,
	[apell_emis] [varchar](50) NULL,
	[dni_emis] [int] NULL,
	[domicilio_emis] [varchar](50) NULL,
	[loc_emis] [varchar](50) NULL,
	[cp_emis] [int] NULL,
	[telefono] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idsolicitud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[envios]    Script Date: 12/17/2019 6:00:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[envios](
	[idenvio] [int] IDENTITY(1,1) NOT NULL,
	[idsolici_recep] [int] NULL,
	[costo] [float] NULL,
	[idsolicitud] [int] NULL,
	[idtipo] [int] NULL,
	[idpeso] [int] NULL,
	[iddistancia] [int] NULL,
	[fecha] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[idenvio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pes]    Script Date: 12/17/2019 6:00:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pes](
	[idpeso] [int] IDENTITY(1,1) NOT NULL,
	[peso] [varchar](20) NULL,
	[precio_pes] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[idpeso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[receptor]    Script Date: 12/17/2019 6:00:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[receptor](
	[idsolici_recep] [int] IDENTITY(1,1) NOT NULL,
	[nomb_recep] [varchar](50) NULL,
	[apell_recep] [varchar](50) NULL,
	[dni_recep] [int] NULL,
	[domicilio_recep] [varchar](50) NULL,
	[loc_recep] [varchar](50) NULL,
	[cp_recep] [int] NULL,
	[telefono] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idsolici_recep] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipoenvio]    Script Date: 12/17/2019 6:00:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipoenvio](
	[idtipo] [int] IDENTITY(1,1) NOT NULL,
	[tipoenv] [varchar](20) NULL,
	[precio] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[idtipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[dis] ON 

INSERT [dbo].[dis] ([iddistancia], [distancia], [precio_dis]) VALUES (1, N'1-50Km', 150)
INSERT [dbo].[dis] ([iddistancia], [distancia], [precio_dis]) VALUES (2, N'51-100Km', 200)
INSERT [dbo].[dis] ([iddistancia], [distancia], [precio_dis]) VALUES (3, N'Mas 100Km', 500)
SET IDENTITY_INSERT [dbo].[dis] OFF
SET IDENTITY_INSERT [dbo].[emisor] ON 

INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (1, N'Gonzalo', N'Lopez', 12589963, N'Callao 578', N'CABA', 1620, 1132254177)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (2, N'Ezequiel', N'Martinez', 215578, N'Azuaga 2014', N'9 de Julio', 1025, 1169875463)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (3, N'lucas', N'gonzales', 364494, N'Loma 45', N'Padua', 1722, 48454451)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (5, N'Juan', N'Gonzalez', 54554, N'Materia 54', N'Merlo', 0, 555)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (6, N'Marina', N'Marquez', 8989, N'Looudes 554', N'Mariano Acosta', 0, 9898)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (7, N'Maria', N'Beltran', 5445, N'loasd 877', N'Merlo', 0, 54548788)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (8, N'Maria', N'Angeles', 8787, N'Mariano 89', N'Merlo', 0, 4546)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (9, N'Ezeequiel', N'Matinez', 78945, N'Azuaga 87', N'Merlo', 0, 5152)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (10, N'Martin', N'Lucre', 579546, N'Mitre 58', N'Flores', 0, 5455454)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (11, N'Gilda ', N'Mirta', 8787, N'sddsds 88', N'Temperley', 0, 544554)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (12, N'rert', N'uhhiuhu', 66465, N'knkj', N'kjnkj', 0, 161651)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (13, N'Lola', N'Martinez', 45454, N'Marn 87', N'Lomas de Zamora', 0, 6554)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (14, N'Juan', N'Artinez', 789455, N'mire 65', N'Merlo', 656, 8455)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (15, N'Mariano', N'Ferreira', 8787454, N'Moreno 87', N'Ituzaingo', 5454, 54544544)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (16, N'Esteban', N'Rodriguez', 6655, N'fsfs 545', N'Merlo', 6565, 656)
INSERT [dbo].[emisor] ([idsolicitud], [nomb_emis], [apell_emis], [dni_emis], [domicilio_emis], [loc_emis], [cp_emis], [telefono]) VALUES (1005, N'dsfihiuh', N'iuhihhh', 8977987, N'uygu', N'yugyyg', 87678, 8776786)
SET IDENTITY_INSERT [dbo].[emisor] OFF
SET IDENTITY_INSERT [dbo].[envios] ON 

INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (1, 1, 400, 1, 2, 1, 1, CAST(N'2019-08-11T18:05:02.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (2, 2, 750, 2, 2, 1, 3, CAST(N'2019-10-07T18:05:02.007' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (3, 3, 250, 3, 2, 1, 1, CAST(N'2019-03-25T20:15:22.017' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (5, 5, 450, 5, 2, 2, 2, CAST(N'2019-11-26T18:16:30.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (6, 6, 600, 6, 1, 1, 3, CAST(N'2019-11-26T18:26:49.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (7, 7, 250, 7, 1, 1, 1, CAST(N'2019-11-26T18:31:45.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (8, 8, 300, 8, 1, 2, 1, CAST(N'2019-11-26T18:50:04.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (9, 9, 400, 9, 2, 1, 2, CAST(N'2019-11-27T16:18:59.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (10, 10, 450, 10, 2, 2, 2, CAST(N'2019-11-27T16:25:36.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (11, 11, 450, 11, 2, 2, 2, CAST(N'2019-11-27T16:34:24.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (12, 12, 250, 12, 1, 1, 1, CAST(N'2019-11-27T16:36:10.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (13, 13, 400, 13, 2, 1, 2, CAST(N'2019-11-27T18:04:00.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (14, 14, 450, 14, 2, 2, 2, CAST(N'2019-11-27T18:14:16.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (15, 15, 450, 15, 2, 2, 2, CAST(N'2019-11-27T18:40:16.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (16, 16, 250, 16, 1, 1, 1, CAST(N'2019-11-27T19:30:02.000' AS DateTime))
INSERT [dbo].[envios] ([idenvio], [idsolici_recep], [costo], [idsolicitud], [idtipo], [idpeso], [iddistancia], [fecha]) VALUES (1005, NULL, 400, NULL, 2, 2, 1, CAST(N'2019-12-04T20:50:22.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[envios] OFF
SET IDENTITY_INSERT [dbo].[pes] ON 

INSERT [dbo].[pes] ([idpeso], [peso], [precio_pes]) VALUES (1, N'1-30kg', 100)
INSERT [dbo].[pes] ([idpeso], [peso], [precio_pes]) VALUES (2, N'31-60kg', 150)
SET IDENTITY_INSERT [dbo].[pes] OFF
SET IDENTITY_INSERT [dbo].[receptor] ON 

INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (1, N'Juan', N'Rodriguez', 21589963, N'Campora 2012', N'Merlo', 1722, 1168254178)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (2, N'Martin', N'Andrada', 11258935, N'Sarmiento 1010', N'Marcos Paz', 1722, 112587963)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (3, N'Agustin', N'Castillo', 2458856, N'Peru 2586', N'Moron', 1722, 65656564)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (5, N'Juan', N'Gonzalez', 54554, N'Materia 54', N'Merlo', 0, 555)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (6, N'Marina', N'Marquez', 8989, N'Looudes 554', N'Mariano Acosta', 0, 9898)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (7, N'Maria', N'Beltran', 5445, N'loasd 877', N'Merlo', 0, 54548788)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (8, N'Louder', N'Victoria', 2546546, N'Savedra 89', N'CABA', 0, 56511)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (9, N'Luis', N'Alberto', 87874, N'Callao8', N'CABA', 0, 5155499)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (10, N'Agustin', N'Gonzalez', 5848, N'lola 89', N'Bahia Blanca', 0, 8898)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (11, N'Martin', N'Gonzalez', 878454, N'olzabal 78', N'Ituzaingo', 0, 5454)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (12, N'kbkbjb', N'kbkjbkjb', 531556, N'kjhh', N'kjbhkh', 0, 65454)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (13, N'Marina', N'Black', 54545, N'9 de julio 21', N'Moron', 0, 545)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (14, N'Carla', N'Gonzale', 56645, N'Lourdes 87', N'CABA', 666, 5454)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (15, N'Lucrecia ', N'Bueno', 87454, N'Callo 88', N'CABA', 5878, 4545)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (16, N'Lucas ', N'martinez', 56465, N'vsdf 87', N'Moron', 6565, 5665)
INSERT [dbo].[receptor] ([idsolici_recep], [nomb_recep], [apell_recep], [dni_recep], [domicilio_recep], [loc_recep], [cp_recep], [telefono]) VALUES (1005, N'komnin', N'uggg', 46464, N'uyuyt', N'ojiguygy', 545, 6454)
SET IDENTITY_INSERT [dbo].[receptor] OFF
SET IDENTITY_INSERT [dbo].[tipoenvio] ON 

INSERT [dbo].[tipoenvio] ([idtipo], [tipoenv], [precio]) VALUES (1, N'Normal', 0)
INSERT [dbo].[tipoenvio] ([idtipo], [tipoenv], [precio]) VALUES (2, N'Express', 100)
SET IDENTITY_INSERT [dbo].[tipoenvio] OFF
ALTER TABLE [dbo].[envios]  WITH CHECK ADD  CONSTRAINT [fk_iddistancia] FOREIGN KEY([iddistancia])
REFERENCES [dbo].[dis] ([iddistancia])
GO
ALTER TABLE [dbo].[envios] CHECK CONSTRAINT [fk_iddistancia]
GO
ALTER TABLE [dbo].[envios]  WITH CHECK ADD  CONSTRAINT [fk_idpeso] FOREIGN KEY([idpeso])
REFERENCES [dbo].[pes] ([idpeso])
GO
ALTER TABLE [dbo].[envios] CHECK CONSTRAINT [fk_idpeso]
GO
ALTER TABLE [dbo].[envios]  WITH CHECK ADD  CONSTRAINT [FK_idsolici_recep] FOREIGN KEY([idsolici_recep])
REFERENCES [dbo].[receptor] ([idsolici_recep])
GO
ALTER TABLE [dbo].[envios] CHECK CONSTRAINT [FK_idsolici_recep]
GO
ALTER TABLE [dbo].[envios]  WITH CHECK ADD  CONSTRAINT [fk_idsolicitud] FOREIGN KEY([idsolicitud])
REFERENCES [dbo].[emisor] ([idsolicitud])
GO
ALTER TABLE [dbo].[envios] CHECK CONSTRAINT [fk_idsolicitud]
GO
ALTER TABLE [dbo].[envios]  WITH CHECK ADD  CONSTRAINT [fk_idtipo] FOREIGN KEY([idtipo])
REFERENCES [dbo].[tipoenvio] ([idtipo])
GO
ALTER TABLE [dbo].[envios] CHECK CONSTRAINT [fk_idtipo]
GO
USE [master]
GO
ALTER DATABASE [CorreoExpressBBDD] SET  READ_WRITE 
GO
