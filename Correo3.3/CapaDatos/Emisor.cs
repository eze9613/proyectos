﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class Emisor
    {
        string nom;
        string ape;
        int dniclien;
        string domic;
        string local;
        int cp;
        int tel;

        public string Nom { get; set; }
        public string Ape { get; set; }
        public int Dniclien { get; set; }
        public string Domic { get; set; }
        public string Local { get; set; }
        public int Cp { get; set; }
        public int Tel { get; set; }


    }
}
